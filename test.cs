using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Framework;


namespace Game
{
    public partial class Play : Form
    {
        public Play()
        {
            InitializeComponent();
        }

        private void load()
        {
            Games e;
            e = Games.etinstance();
            Factory f;
            f = Factory.createInstance();
            GameObjects enemy1 = f.load(pictureBox2,10,FactoryMovement.move.left,ObjectType.objectType.enemy);
            GameObjects enemy2 = f.load(pictureBox3, 10, ObjectType.objectType.enemy);
            GameObjects enemy3 = f.load(pictureBox5, 40, FactoryMovement.move.left, ObjectType.objectType.enemy);
            GameObjects enemy4 = f.load(pictureBox4, 10, FactoryMovement.move.right, ObjectType.objectType.enemy);
             //player1 = f.load(player,15,new KeyBoardControle(),ObjectType.objectType.player);
            e.addRecord(enemy1);
            e.addRecord(enemy2);
            e.addRecord(enemy3);
            e.addRecord(enemy4);
            
        }

        //private Movement get_move(FactoryMovement.move state)
        //{
        //    FactoryMovement obj;
        //    obj = FactoryMovement.getInstance();
        //    Movement mave = obj.getsource(state);
        //    return mave;
        //}

        private void GameLoop_Tick(object sender, EventArgs e)
        {
            Games E;
            E = Games.etinstance();
            E.updateEnemy();
            CollisionDetection c;
            c = CollisionDetection.getInstance();
            c.playerCollision(player,pictureBox2, Reaction.effect.destroy);
            c.playerCollision(player, pictureBox3, Reaction.effect.destroy);
            c.playerCollision(player, pictureBox4, Reaction.effect.destroy);
            c.playerCollision(player, pictureBox5, Reaction.effect.destroy);
            Left l;
            l = Framework.Left.getinstance();
            int idx = l.getindex();
            label2.Visible = true;
            label2.Text = idx.ToString();
        }

        private void win()
        {
            if (player.Bounds.IntersectsWith(label1.Bounds) || player.Bounds.IntersectsWith(label2.Bounds) || player.Bounds.IntersectsWith(label3.Bounds) && label1.Visible == true)
            {
                label1.Location = new Point(424, 128);
                label1.Text = "YOU WIN";
                label2.Visible = false;
                label3.Visible = false;
            }
        }

        //public void load_player()
        //{
        //    Player p;
        //    p = Player.getinstance();
        //    PlayerObject player1 = new PlayerObject(player, 10);
        //    p.addRecord(player1);
        //}

        public void Play_KeyDown(object sender, KeyEventArgs e)
        {
            //Player p;
            //p = Player.getinstance();
            //p.updatePlayer(player,e,sender);
            //check();
        //    Games g;
        //    g = Games.etinstance();
        //    g.updateEnemy(e, 20, player);
        }

        private void win_Loop_Tick(object sender, EventArgs e)
        {
            this.label1.Visible = true;
            this.label2.Visible = true;
            this.label3.Visible = true;
            win();
        }

        private void GameOver()
        {
            //label1.Text = "GAME OVER";
            //label1.Location = new Point(424, 128);
            //label2.Visible = false;
            //label3.Visible = false;
        }
        

        private void check()
        {
            foreach (Control item in this.Controls)
            {
                if(item is PictureBox)
                {
                    if (player.Location == item.Location || item.Left == player.Left || item.Top == player.Top )
                    {
                            GameOver();
                    }
                    
                    if (player.Bounds.IntersectsWith(item.Bounds))
                    {
                        GameOver();
                    }

                }
            }
        }

        private void upside()
        {
            int s = player.Top; 
            if (pictureBox2.Left > 1166)
            {
                pictureBox2.Location = new Point(1,37);
                pictureBox2.Left = pictureBox2.Left+10;
            }
            if (pictureBox3.Left > 1166)
            {
                pictureBox3.Location = new Point(1,300);
                pictureBox3.Left = pictureBox3.Left + 10;
            }
            if (pictureBox5.Left > 1166)
            {
                pictureBox5.Location = new Point(1, s);
                pictureBox5.Left = pictureBox5.Left + 30;
            }
            if (pictureBox4.Left < 10)
            {
                pictureBox4.Location = new Point(1166,s);
                pictureBox4.Left = pictureBox4.Left - 30;
            }
            check();
            
        }

        private void Play_Load(object sender, EventArgs e)
        {
            load();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            //FactoryMovement obj;
            //obj = FactoryMovement.getInstance();
            //Movement m = obj.newMovement(FactoryMovement.move.backward);
            //obj.release(m);
        }

    }
}
